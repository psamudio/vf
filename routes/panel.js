var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var user = req.session.usuario;
	if (user) {
  res.render('panel', { usuario:user });
}else{
	res.redirect('/login');
}

});
router.get('/logout', function(req,res,next){
	//Cerrar session req.session.destroy
	// req session nombre
	// si a destroy no le paso parametros. se destruyen todas las sesiones activas
	req.session.destroy()
	res.redirect('/login');

})

module.exports = router;
