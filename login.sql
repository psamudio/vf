-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-12-2018 a las 01:04:06
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `login`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(22) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `precio` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `precio`) VALUES
(1, 'Lomo', '80.00'),
(2, 'Costillas', '99.99'),
(3, 'Churrascos', '99.99'),
(4, 'Bifecito de cuadril', '110.00'),
(5, 'Colita de cuadril a la crema de puerros', '200.00'),
(6, 'Lomo a la mostaza', '79.00'),
(7, 'Klops de carne con cubos de calabaza', '110.00'),
(8, 'Albondigas', '100.00'),
(9, 'Cazuela Campesina c pollo', '150.00'),
(10, 'Latte', '90.00'),
(11, 'Suprema', '80.00'),
(12, 'Supremas Rellenas', '50.00'),
(13, 'Lomitos de pollo al verdeo', '120.00'),
(14, 'Berenjena a la Napolitana', '100.00'),
(15, 'Pollo con verduras', '100.00'),
(16, 'Canelones de verdura', '100.00'),
(17, 'Canelones de verdura', '100.00'),
(18, 'Carbonada', '100.00'),
(19, 'Cazuela de lentejas', '100.00'),
(20, 'Empanaditas caprese con puré verdeo', '80.00'),
(21, 'Adicionales: pollo/atun/crutones/grisines', '40.00'),
(22, 'Cesar', '120.00'),
(23, 'Lechuga tomate', '130.00'),
(24, 'Rucula parmesano', '130.00'),
(25, 'RBudin de Pescado', '130.00'),
(26, 'Chupín de pescado', '130.00'),
(27, 'Filet a la Florentina con Puré Mixto', '130.00'),
(28, 'Filet con queso crema', '130.00'),
(29, 'Buñuelos de pescado', '130.00'),
(30, 'Bloody Mary', '130.00'),
(31, 'A la romana con limon', '130.00'),
(32, 'Lasaña', '130.00'),
(33, 'Ñoquis de calabaza', '130.00'),
(34, 'Ravioles con salsa de Tomate', '130.00'),
(35, 'Risotto a la crema con champiñones', '130.00'),
(36, 'Ñoquis de papa', '130.00'),
(37, 'Capeletttis', '130.00'),
(38, 'Canelones', '130.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(22) NOT NULL,
  `usuario` varchar(40) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `password`) VALUES
(1, 'ps', '123'),
(5, 'samudiopabloe@gmail.com', '123'),
(6, 'a@a.a', 'a@a.a'),
(7, 'samudiopabloe@gmail.com.ar', '123'),
(8, 'sa@salchipapa.com', '123456'),
(9, 'test@test.test', '123'),
(10, 'a@a.com', '123'),
(11, 'a@a.a', '123'),
(12, 'ROCIO.MEDINA@TE-AMO.MUCHO', '123'),
(13, 'sa@D.com', '123');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
