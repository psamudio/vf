var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

    var session = require('express-session');
    var consulta = require('./routes/consultar');
    var carro = require ('./routes/carro');
    var precios = require ('./routes/precios');
    
    var login = require ('./routes/login');
    var carroLOG = require ('./routes/carroLOG');
    var panel = require ('./routes/panel');
    var registro = require('./routes/registro');
    var buscar = require('./routes/buscar');
    //


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
	
	
   

app.use('/', indexRouter);
app.use('/users', usersRouter);
//variables de sesion
  app.use(session({secret:'123456',cookie:{maxAge:null}}));
//  
  app.use('/consultar', consulta);
  app.use('/carro',carro);
  app.use('/precios',precios);
  app.use('/login',login);
  app.use('/carroLOG',carroLOG);
  app.use('/registro',registro);
  app.use('/buscar',buscar);
  
  app.use('/panel',panel);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
//
